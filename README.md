完美解决Android中的ScrollView嵌套ScrollView滑动冲突问题
参考:[输入链接说明](https://blog.csdn.net/Ever69/article/details/104315253?utm_medium=distribute.pc_relevant.none-task-blog-baidujs_baidulandingword-0&spm=1001.2101.3001.4242)

使用
 内部嵌套的ScrollView, 只能ScrollView中嵌套一层InnerScrollView


```
dependencies {
	 implementation 'com.gitee.kavin_tian:InnerScrollView:1.0.0'
    }
```


 伪代码
```
  <ScrollView>
      <InnerScrollView>
       </InnerScrollView>
  </ScrollView>
```

