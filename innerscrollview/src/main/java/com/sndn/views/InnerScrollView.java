package com.sndn.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewParent;
import android.widget.ScrollView;

/**
 * 内部嵌套的ScrollView, InnerScrollView上可以多层ScrollView
 * <ScrollView>
 *    <HorizontalScrollView>
 *        <InnerScrollView>
 *        </InnerScrollView>
 *    </HorizontalScrollView>
 * </ScrollView>
 */
public class InnerScrollView extends ScrollView {

    public InnerScrollView(Context context) {
        super(context);
    }

    public InnerScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public InnerScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private int getMaxY() {
        int maxY = -1;
        if (getChildCount() > 0 && maxY < 0) {
            maxY = getChildAt(0).getMeasuredHeight() - getHeight();
        }
        return maxY;
    }

    //在Y轴上可以滑动的最大距离 = 总长度 - 当前展示区域长度
    private int mMaxY = -1;
    float downX = 0;
    float downY = 0;
    boolean ScrollEnabled = false;

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                downX = ev.getX();
                downY = ev.getY();

                //获取父控件的父控件,对应外层ScrollView的节点 不拦截事件
                //getParent().getParent().requestDisallowInterceptTouchEvent(true);
                mMaxY = getMaxY();
                break;
            case MotionEvent.ACTION_MOVE:
                float moveX = ev.getX();
                float moveY = ev.getY();

                float x = moveX - downX;
                float y = moveY - downY;

                if (!ScrollEnabled) {
                    //兼容嵌套HorizontalScrollView
                    if (Math.abs(y) >= Math.abs(x)) {
                        //Parent递归不拦截事件
                        interceptTouchEvent(this);
                        ScrollEnabled = true;
                    }
                }

                if (ScrollEnabled) {
                    //滑到头 或者 滑到底部,请求外部的ScrollView拦截事件
                    if (getScrollY() <= 0 || getScrollY() >= mMaxY) {
                        //false拦截是递归调用
                        getParent().requestDisallowInterceptTouchEvent(false);
                    }
                }

                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                getParent().requestDisallowInterceptTouchEvent(false);
                ScrollEnabled = false;
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    /**
     * 请求所有的ScrollView父控件不拦截事件
     */
    private void interceptTouchEvent(ViewParent viewParent) {
        //getParent().getParent().requestDisallowInterceptTouchEvent(true);
        ViewParent parent = viewParent.getParent();
        if (parent == null) {
            return;
        }
        //LogUtils.e("--------interceptTouchEvent", parent.toString());
        if (parent instanceof ScrollView) {
            parent.requestDisallowInterceptTouchEvent(true);
        }
        interceptTouchEvent(parent);

    }
}